scalaVersion := "2.12.8"

name := "exam"
organization := "ar.com.juani"
version := "1.0"

libraryDependencies ++= Seq(
  "org.jsoup" % "jsoup" % "1.11.2",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test",
  "org.scalamock" %% "scalamock" % "4.1.0" % "test",
)