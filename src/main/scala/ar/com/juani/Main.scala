package ar.com.juani

import java.io.File

import org.jsoup.Jsoup

object Main extends App {

  override def main(args: Array[String]): Unit = {

    if (args.length < 2) {
      print(s"Usage: <input_origin_file_path> <input_other_sample_file_path> [tag_id]")
      return
    }

    val tagId = if (args.length > 2) args(2) else "make-everything-ok-button"

    val origin = new File(args(0))
    val sample = new File(args(1))
    val analyzer = new Analyzer(Jsoup.parse(origin, "UTF-8"), Jsoup.parse(sample, "UTF-8"))

    analyzer.findMostSimilar(tagId) match {
      case Some(result) =>
        print(s"Result found at: ${result.location}")
      case _ => print("Result not found")
    }
  }
}