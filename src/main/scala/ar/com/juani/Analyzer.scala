package ar.com.juani

import collection.JavaConverters._

import org.jsoup.nodes.{Document, Element}

class Analyzer(original: Document, variation: Document) {

  def findMostSimilar(targetId: String): Option[AnalyzerElement] = {
    Option(original.getElementById(targetId)).flatMap {
      el =>
        val target = new AnalyzerElement(el)
        val candidates = variation.select("*").iterator().asScala.toList.map(el => new AnalyzerElement(el))

        candidates.sortBy(_.similarityScore(target)).reverse.headOption
    }
  }
}

class AnalyzerElement(val el: Element) {
  lazy val attributes: Map[String, String] = el.attributes.asList.asScala.map(a => (a.getKey, a.getValue)).toMap

  lazy val textFragments: Set[String] = el.textNodes().asScala.map(_.getWholeText).toSet

  def id: String = el.id

  def text: String = textFragments.mkString("").trim

  def classes: Set[String] = attributes.getOrElse("class", "").split("\\s").toSet

  def similarityScore(targetElement: AnalyzerElement): BigDecimal = {
    val scoreForId = if (id == targetElement.id) 100 else 0
    val scoreForText = if (textFragments.intersect(targetElement.textFragments).nonEmpty) 60 else 0
    val scoreForCommonClasses = targetElement.classes.intersect(this.classes).size * 10

    scoreForId + scoreForText + scoreForCommonClasses
  }

  def location: String = el.cssSelector
}