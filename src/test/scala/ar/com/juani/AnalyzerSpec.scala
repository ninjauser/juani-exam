package ar.com.juani

import org.jsoup.Jsoup
import org.scalatest.{FlatSpec, Matchers}

class AnalyzerSpec extends FlatSpec with Matchers {

  val OriginalExcerpt: String =
    """
      |                     <div class="panel-heading">
      |                            <i class="fa fa-chain-broken fa-fw"></i> Make Everything OK Area
      |                            <div class="pull-right">
      |                                <div class="btn-group">
      |                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
      |                                        Actions
      |                                        <span class="caret"></span>
      |                                    </button>
      |                                    <ul class="dropdown-menu pull-right" role="menu">
      |                                        <li><a href="#">Action</a>
      |                                        </li>
      |                                        <li><a href="#">Another action</a>
      |                                        </li>
      |                                        <li><a href="#">Something else here</a>
      |                                        </li>
      |                                        <li class="divider"></li>
      |                                        <li><a href="#">Separated link</a>
      |                                        </li>
      |                                    </ul>
      |                                </div>
      |                            </div>
      |                        </div>
      |                        <!-- /.panel-heading -->
      |                        <div class="panel-body">
      |                            <a
      |                                id="make-everything-ok-button"
      |                                class="btn btn-success"
      |                                href="#ok"
      |                                title="Make-Button"
      |                                rel="next"
      |                                onclick="javascript:window.okDone(); return false;">
      |                              Make everything OK
      |                            </a>
      |                        </div>
      |                        <!-- /.panel-body -->
      |                    </div>
    """.stripMargin



  "An Analyzer" should "work with case 3" in {

    val variation: String =
      """
        |<div class="col-lg-8">
        |                    <div class="panel panel-default">
        |                        <div class="panel-heading">
        |                            <i class="fa fa-chain-broken fa-fw"></i> Make Everything OK Area
        |                            <div class="pull-right">
        |                                <div class="btn-group">
        |                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
        |                                        Actions
        |                                        <span class="caret"></span>
        |                                    </button>
        |                                    <ul class="dropdown-menu pull-right" role="menu">
        |                                        <li><a href="#">Action</a>
        |                                        </li>
        |                                        <li><a href="#">Another action</a>
        |                                        </li>
        |                                        <li><a href="#">Something else here</a>
        |                                        </li>
        |                                        <li class="divider"></li>
        |                                        <li><a href="#">Separated link</a>
        |                                        </li>
        |                                    </ul>
        |                                </div>
        |                            </div>
        |                        </div>
        |                        <!-- /.panel-heading -->
        |                        <div class="panel-body">
        |                          <a href="#ok" class="btn btn-warning" rel="next" onclick="javascript:window.close(); return false;">
        |                            Make something somehow
        |                          </a>
        |                        </div>
        |                        <!-- /.panel-body -->
        |                        <div class="panel-footer">
        |                          <a
        |                              class="btn btn-success"
        |                              href="#ok"
        |                              title="Do-Link"
        |                              rel="next"
        |                              onclick="javascript:window.okDone(); return false;">
        |                            Do anything perfect
        |                          </a>
        |                        </div>
        |                        <!-- /.panel-footer -->
        |                    </div>
        |                </div>
      """.stripMargin

    val analyzer = new Analyzer(Jsoup.parse(OriginalExcerpt), Jsoup.parse(variation))
    val result = analyzer.findMostSimilar("make-everything-ok-button")

    result.get.text shouldBe "Do anything perfect"
  }

  it should "work with case 1" in {

    val variation: String =
      """
        |<div class="col-lg-8">
        |                    <div class="panel panel-default">
        |                        <div class="panel-heading">
        |                            <i class="fa fa-chain-broken fa-fw"></i> Make Everything OK Area
        |                            <div class="pull-right">
        |                                <div class="btn-group">
        |                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
        |                                        Actions
        |                                        <span class="caret"></span>
        |                                    </button>
        |                                    <ul class="dropdown-menu pull-right" role="menu">
        |                                        <li><a href="#">Action</a>
        |                                        </li>
        |                                        <li><a href="#">Another action</a>
        |                                        </li>
        |                                        <li><a href="#">Something else here</a>
        |                                        </li>
        |                                        <li class="divider"></li>
        |                                        <li><a href="#">Separated link</a>
        |                                        </li>
        |                                    </ul>
        |                                </div>
        |                            </div>
        |                        </div>
        |                        <!-- /.panel-heading -->
        |                        <div class="panel-body">
        |                            <a
        |                                class="btn btn-danger"
        |                                href="#ok"
        |                                title="Make-Button"
        |                                onclick="javascript:window.close(); return false;">
        |                              Break everyone's OK
        |                            </a>
        |
        |                            <a
        |                                class="btn btn-success"
        |                                href="#check-and-ok"
        |                                title="Make-Button"
        |                                rel="done"
        |                                onclick="javascript:window.okDone(); return false;">
        |                              Make everything OK
        |                            </a>
        |                        </div>
        |                        <!-- /.panel-body -->
        |                    </div>
        |                </div>
      """.stripMargin

    val analyzer = new Analyzer(Jsoup.parse(OriginalExcerpt), Jsoup.parse(variation))
    val result = analyzer.findMostSimilar("make-everything-ok-button")

    result.get.text shouldBe "Make everything OK"
  }

  it should "work with case 2" in {

    val variation =
      """
        |<div class="panel panel-default">
        |                        <div class="panel-heading">
        |                            <i class="fa fa-chain-broken fa-fw"></i> Make Everything OK Area
        |                            <div class="pull-right">
        |                                <div class="btn-group">
        |                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
        |                                        Actions
        |                                        <span class="caret"></span>
        |                                    </button>
        |                                    <ul class="dropdown-menu pull-right" role="menu">
        |                                        <li><a href="#">Action</a>
        |                                        </li>
        |                                        <li><a href="#">Another action</a>
        |                                        </li>
        |                                        <li><a href="#">Something else here</a>
        |                                        </li>
        |                                        <li class="divider"></li>
        |                                        <li><a href="#">Separated link</a>
        |                                        </li>
        |                                    </ul>
        |                                </div>
        |                            </div>
        |                        </div>
        |                        <!-- /.panel-heading -->
        |                        <div class="panel-body">
        |                          <div class="some-container">
        |                            <a
        |                                class="btn test-link-ok"
        |                                href="#ok"
        |                                title="Make-Button"
        |                                rel="next"
        |                                onclick="javascript:window.okComplete(); return false;">
        |                              Make everything OK
        |                            </a>
        |                          </div>
        |                        </div>
        |                        <!-- /.panel-body -->
        |                    </div>
      """.stripMargin

    val analyzer = new Analyzer(Jsoup.parse(OriginalExcerpt), Jsoup.parse(variation))
    val result = analyzer.findMostSimilar("make-everything-ok-button")

    result.get.text shouldBe "Make everything OK"
  }

  it should "work with case 4" in {

    val variation =
      """
        |<div class="col-lg-8">
        |                    <div class="panel panel-default">
        |                        <div class="panel-heading">
        |                            <i class="fa fa-chain-broken fa-fw"></i> Make Everything OK Area
        |                            <div class="pull-right">
        |                                <div class="btn-group">
        |                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
        |                                        Actions
        |                                        <span class="caret"></span>
        |                                    </button>
        |                                    <ul class="dropdown-menu pull-right" role="menu">
        |                                        <li><a href="#">Action</a>
        |                                        </li>
        |                                        <li><a href="#">Another action</a>
        |                                        </li>
        |                                        <li><a href="#">Something else here</a>
        |                                        </li>
        |                                        <li class="divider"></li>
        |                                        <li><a href="#">Separated link</a>
        |                                        </li>
        |                                    </ul>
        |                                </div>
        |                            </div>
        |                        </div>
        |                        <!-- /.panel-heading -->
        |                        <div class="panel-body">
        |                          <button class="btn btn-success" onclick="javascript:location='http://google.com';" style="display:none">
        |                            Make everything OK
        |                          </button>
        |                        </div>
        |                        <!-- /.panel-body -->
        |                        <div class="panel-footer">
        |                          <a
        |                              class="btn btn-success"
        |                              href="#ok"
        |                              title="Make-Button"
        |                              rel="next"
        |                              onclick="javascript:window.okFinalize(); return false;">
        |                            Do all GREAT
        |                          </a>
        |                        </div>
        |                        <!-- /.panel-footer -->
        |                    </div>
        |                </div>
      """.stripMargin

    val analyzer = new Analyzer(Jsoup.parse(OriginalExcerpt), Jsoup.parse(variation))
    val result = analyzer.findMostSimilar("make-everything-ok-button")

    result.get.text shouldBe "Do all GREAT"
  }
}
